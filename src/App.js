import { Slots } from './slot';

function App() {
  return (
    <div>
      <Slots/>
    </div>
  );
}

export default App;
